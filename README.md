This all started simply enough. I wanted to brand the SAML IdP's user constent form (the form prompting people to consent to releasing their information) using the login.case.edu markup and CSS, but I ran into problems like fixed height in pixels in the CSS, user agent sniffing, and confusing nested div's in the markup.

After stripping out what I could, so I had a framework to use to put in the consent form; I ended up with little left but an img tag and some markup for the banner. What began as a quick side-task, turned into this project.

See [the wiki](https://bitbucket.org/jms18/login-ui/wiki) for all the nitty-gritty.

Live versions of the pages are at [https://oauth.case.edu/login](https://oauth.case.edu/login).